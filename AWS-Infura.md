<div align="center">
  <h1>
    Liquid Crypto AWS Infurastructure
  </h1>
</div>

The platform is using 6 AWS/EC2 instances.

<img src="infra/1.png" alt="drawing" style="max-width:800px;"/>

## 1. Mongo DB

This instance is for storing all data - user list, LC pool's APR and TVL, basket list, event history and so on.

Only `Web instance`, `DEX aggregator`, `Bridge Relayer` and `APR aggregator` can access this instance and others are blocked.

## 2. DEX Aggretator

This instance interacting all chains DEXs smart contract and aggregate pool informations.

Aggregated DEX's pool information will be stored in `Mongo DB`.

This information will be used for get best trade route in swap+.

## 3. APR Aggregator 1 & 2

These instances aggregate LC pool's APR by using smart contract and scrapping.

These instances update pool's APR every 15mins.

Also, these instances update token's price every 5mins.

And store all aggregated data into `Mongo DB`

## 4. Bridge Relayer

Bridge Relayer detect user's bridge transfer request based on analyzing event from smart contract.

Then store user's request information into `Mongo DB` and process transfer request interacting bridge smart contracts.

## 5. Web instance

In this instance, front-end, RESTful API and socket server are running.

Front-end is built from Next.js and API is built from Express.

API get data from `Mongo DB`.

Socket server is for updating information automatically in front-end.
