<div align="center">
  <h1>
    Liquid Crypto Basket
  </h1>
</div>

## I. What is the basket?

Basket is group of staking pools.

Users can deposit/withdraw into basket by using a kind of coin.

For example users can deposit BSC pools and Fantom pools by using only BNB at once.

Without basket user need to hold on different kinds of coins/tokens to deposit into different kinds of pools.


## II. What things are need to use basket?

**To deposit and withdraw into/from basket, enough liquidity must be added into bridge.**

Please reference `Bridge.md` if you want to know how to add liquidity.

## III. How to process deposit into the basket?

<img src="basketv3/1.png" alt="drawing" style="max-width:800px;"/>

This basket includes four BSC pools and two fantom pools.

If a user trying to deposit into these 3 pools with 6 BNB, process will be performed as follow step.

1. User deposit 6 BNB into BSC basket smart contract.
2. 6 BNB is splitted as 4 BNB for 4 BSC pools and 2 BNB for 2 Fantom pools.

   4 BNB is deposited 1 BNB per BSC pools.

   2 BNB is deposited into bridge to deposit 2 fantom pools.

   If LC birdge has enough liquidity, 2 BNB will be swapped as FTM via LC bridge.

   If LC bridge has not enough liquidity, 2 BNB will be swapped as FTM via Stargate bridge.
3. Bakset contract will get 4 kinds of LP tokens from 4 BSC pools.
4. These 4 kinds of BSC pools LP token will be transfered to depositior.
5. When bridge swapping is occured, `swap` event will be transfered to `operator`.
6. `Operator` analyze how much depositor deposited, then deposit 2 Fantom pools.
7. When `operator` deposit into basket instead of depositer, swapped asset will be transfered from the bridge.

   If bridge swap is performed via LC bridge, wrapped FTM will be transfered to basket contract.

   If bridge swap is performed via Stargate bridge, stable token will be transfered to basket contract and will be swapped as wrapped FTM via other DEX smart contract (i.e Spookyswap ...).
8. Fantom basket smart contract will deposit into 2 Fantom pools with wrapped FTM that be swpped via bridge from splitted 2 BNB.
9. Basket smart contract will get 2 kinds of fantom pools LP token.
10. These LP tokens will be transfered to depositor.

## IV. How to process withdraw from the basket?

Withdraw operation will be performed similar as depositor.

When user withdraw from baskest, all pools assets included in basket will be withraw.

Then assets in different chains from withdraw will be swapped via LC bridge or Stargate, then transfered to user.

## V. Basket Contract Deployed addresses

| Chain | Address
| ----------- | --------- |
| BSC | [0xe79715fC505096Bbc94D0bB6aa23Cf91c116d698](https://bscscan.com/address/0xe79715fC505096Bbc94D0bB6aa23Cf91c116d698) |
| Fantom | [0xb9f95E1D0Ff3e53Ba981C1B14Ea75a85b6b6f4Ee](https://ftmscan.com/address/0xb9f95E1D0Ff3e53Ba981C1B14Ea75a85b6b6f4Ee) |
| Avalanche | [0x72c88961666B21Aaf0017eB4a46865d0970c28F6](https://snowtrace.io/address/0x72c88961666B21Aaf0017eB4a46865d0970c28F6) |
| Polygon | [0xB3B28341E129dBD50c29E35Cf448D50530aEe37d](https://polygonscan.com/address/0xB3B28341E129dBD50c29E35Cf448D50530aEe37d) |
| Ethereum | [0xfc2e260B7E582aa9a0DAdD9A42BF02FF472a0e72](https://etherscan.io/address/0xfc2e260B7E582aa9a0DAdD9A42BF02FF472a0e72) |

## VI. Nwe Basket Contract Deployed addresses

| Chain | Address |
| ----- | ------- |
| BSC | [0xdfD56D775E825B91ADFebf7c701476911B57317b](https://bscscan.com/address/0xdfD56D775E825B91ADFebf7c701476911B57317b) |
| Fantom | [0x0c7765A8CF3230B94A60e48997AC947c7faA1bce](https://ftmscan.com/address/0x0c7765A8CF3230B94A60e48997AC947c7faA1bce) |
| Avalanche | [0xeE9123D9f75c8c22e3419EEcE4b8CbAFCB164F8E](https://snowtrace.io/address/0xeE9123D9f75c8c22e3419EEcE4b8CbAFCB164F8E) |
| Polygon | [0xc1Ffd90725d4c59A92E0114420A26dCe1df1888f](https://polygonscan.com/address/0xc1Ffd90725d4c59A92E0114420A26dCe1df1888f) |
| Celo | [0x00bC5B348e70A65e2427e6C787b6d76F4789E90b](https://celoscan.io/address/0x00bC5B348e70A65e2427e6C787b6d76F4789E90b) |
| Cronos | [](https://cronoscan.com/address/) |
| Metis | [0x9925e3E25f026155d69Fe5C7251867BE2d275D93](https://andromeda-explorer.metis.io/address/0x9925e3E25f026155d69Fe5C7251867BE2d275D93/transactions) |
| Moonbeam | [](https://moonscan.io/address/) |
| Ethereum | [0xA502490f9eC1eb7c4483E922c4bd3982eD92CD1D](https://etherscan.io/address/0xA502490f9eC1eb7c4483E922c4bd3982eD92CD1D) |
| ONUS | [0x3298f328b71cc0F3A70939A6A31476D4570CFD8B](https://explorer.onuschain.io/address/0x3298f328b71cc0F3A70939A6A31476D4570CFD8B/contracts) |
| Arbitrum | [0xF2e9441aF84475EC6Ad487cAe445a15F8816aD43](https://arbiscan.io/address/0xF2e9441aF84475EC6Ad487cAe445a15F8816aD43) |
| Boba | [0xaF940f875363206e0Bb55a64A444f54f8c31e3f7](https://bobascan.com/address/0xaF940f875363206e0Bb55a64A444f54f8c31e3f7) |
| Optimism | [0xd6EA8d65BF536c24CAd9cE4b47e8781D4Aa80D68](https://optimistic.etherscan.io/address/0xd6EA8d65BF536c24CAd9cE4b47e8781D4Aa80D68) |
| JFIN | [0xa6Ad5372E46bFE1CB5691c089901ba61d3B94143](https://exp.jfinchain.com/address/0xa6Ad5372E46bFE1CB5691c089901ba61d3B94143) |
| Mantle | [0x20aDbeB0fAFD454F8F6B7100f143Dfda5F95c873](https://explorer.mantle.xyz/address/0x20aDbeB0fAFD454F8F6B7100f143Dfda5F95c873) |
| Base | [0x27DD09b7CAD097d147B458c9900C4c0f8c08beC8](https://basescan.org/address/0x27DD09b7CAD097d147B458c9900C4c0f8c08beC8) |
| OORT | [0x3De4fB2033360598385d14A886d6B097f4209DC7](https://mainnet-scan.oortech.com/address/0x3De4fB2033360598385d14A886d6B097f4209DC7) |
| Arthera | [0x0802dE6AF6bDe9e5938eb05323aa00314f69594a](https://explorer.arthera.net/address/0x0802dE6AF6bDe9e5938eb05323aa00314f69594a) |
