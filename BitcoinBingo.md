<div align="center">
  <h1>
    Liquid Crypto - Bitcoin Bingo Smart contract
  </h1>
</div>


## I. What is the Bitcoin Bingo?

Guess the correct price of Bitcoin each week for your chance to win a minimum of $1000USDC! Simply enter your guess below and if you guess correctly, you win the jackpot! Make sure to be following our socials to see if you are the winner each week

## II. What things are integrated in the smart contract?

1. Get correct BTC/USD price from Chainlink datafeed smart contract.

2. Integrated anti-bot function.

## III. Live URL

https://liquidcrypto.finance/bingo

## IV. Deployed addresses

| Chain | Address
| ----------- | --------- |
| BSC | [0xff3EaF93ea208c79Ba7c664D0b8405a396eF4881](https://bscscan.com/address/0xff3EaF93ea208c79Ba7c664D0b8405a396eF4881) |