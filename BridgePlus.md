<div align="center">
  <h1>
    Liquid Crypto Bridge+
  </h1>
</div>

## I. What is the bridge+?

Bridge+ is group of liquid crypto's bridge, cBridge, Synapse and Stargate bridge.

Users can swap one chain's token for another chain's token via bridge group.

Bridge+ is a candidate for the best bridging system for user's who wish to  swap options across multiple chains.

## II. How to swap in Bridge+?

<img src="bridgeplus/1.png" alt="drawing" style="max-width:800px;"/>

User select source chain and token, then type the amount to swap.

Also, select destination chain and token.

In this case, your trying to swap BSC 0.1BNB to Fantom USDC.

<img src="bridgeplus/routing.png" alt="drawing" style="max-width:800px;"/>

1. When user type amount, the backend validate bridge's balance and select which bridge is suitable.

   Bridge priority is LC bridge, cBridge, Synapse and Stargate bridge.

   If LC bridge has got enough balance, the backend select LC bridge.

   If not, the backend validate other following bridges.

   This process is performed at `getRouter()` function in `controllers/bridgev2.js`.

2. After bridge is selected, user send transaction to recommended bridge.

   a) If LC bridge is selected and user selected coin, the `swap()` function in `LiquidCryptoBridge_v2` contract.

   If LC bridge is selected and user selected token, the `swap()` function in `CBridgeZapv1` contract.

   b) If other bridge is selected, `CBridgeZapv1/swap()`, `StgBridgeZapv1/swap()`, `StgBridgeZapv1/swapFromCoin()` functions will be called.

   These zap contracts cut swapping fee and transfer to treasury wallet.

3. When transaction is confirmed, the transaction is analyzed.

   If user get token from bridge, the swapping is completed in this step.

   If not, process go to next step.

4. If user doesn't get received token from bridge directly, his transaction is sent backend.

   All request is registered via `swaprequest()` function in `controllers/bridgev2.js`.

5. Registered requests are analyzed at `_bridgingRelayer()` function in `cron/process_relayer.js`.

   a) If user swapping via LC bridge, the backend analyze user's transaction and redeem token from other chain's LC bridge smart contract.

   If the desitination token is gas coin, the backend call `redeem()` function in `LiquidCryptoBridge_v2` contract.

   If not, the backend call `redeemAndSwapFromLcBridge()` function in `BridgeReceiverv1` contract.

   b) If user swapping via other bridge and bridge can't redeen user's destination token, bridge will redeem token to `BridgeReceiverv1` contract and swap&redeem to user.

6. After transaction submited, user can get destination token.

## III. LC Bridge Deployed addresses

| Chain | Address
| ----------- | --------- |
| BSC | [0xc45F6C2528a902480B18ff975367f6510EFf17de](https://bscscan.com/address/0xc45F6C2528a902480B18ff975367f6510EFf17de) |
| Fantom | [0xf2fE41C1F83C23A49ee7656d2860b9df117a826F](https://ftmscan.com/address/0xf2fE41C1F83C23A49ee7656d2860b9df117a826F) |
| Avalanche | [0xDD151740aE207de198Af1833C97333A6904E6aFe](https://snowtrace.io/address/0xDD151740aE207de198Af1833C97333A6904E6aFe) |
| Polygon | [0xdbFe773C3300142089404A5f9Ad1f956656e6788](https://polygonscan.com/address/0xdbFe773C3300142089404A5f9Ad1f956656e6788) |
| Celo | [0xADb4A59D08e597a6ED16afC4FC1808C0eFa61F49](https://celoscan.io/address/0xADb4A59D08e597a6ED16afC4FC1808C0eFa61F49) |
| Cronos | [0x1225EB759104090fA7159C8D62D478251B888378](https://cronoscan.com/address/0x1225EB759104090fA7159C8D62D478251B888378) |
| Metis | [0xE8F5417C4312B95613b7442130A7086b687Cf0f5](https://andromeda-explorer.metis.io/address/0xE8F5417C4312B95613b7442130A7086b687Cf0f5/transactions) |
| Moonbeam | [0x7d838a5cd0Beb6Fba7395Ba9613D708BC765839d](https://moonscan.io/address/0x7d838a5cd0Beb6Fba7395Ba9613D708BC765839d) |
| Ethereum | [0x8c8869dF9EA1C3200DB927f7033976a76138E3c4](https://etherscan.io/address/0x8c8869dF9EA1C3200DB927f7033976a76138E3c4) |

## IV. LC Bridge Zap Deployed addresses

| Chain | Address
| ----------- | --------- |
| BSC | [0xB0c723fd39A2315cc90Ce66719aE47FAc436cB43](https://bscscan.com/address/0xB0c723fd39A2315cc90Ce66719aE47FAc436cB43) |
| Fantom | [0x206a2819EC3131AD6b05267e07D0b430A8a501B1](https://ftmscan.com/address/0x206a2819EC3131AD6b05267e07D0b430A8a501B1) |
| Avalanche | [0xA27D035bb7B507F32b5593f4210984598da721Dc](https://snowtrace.io/address/0xA27D035bb7B507F32b5593f4210984598da721Dc) |
| Polygon | [0xa418658b51590865bE77084C97640cB3d6261102](https://polygonscan.com/address/0xa418658b51590865bE77084C97640cB3d6261102) |
| Ethereum | [0x3A5129247306171ddC36ebCBC6e015bD9fd6f3a0](https://etherscan.io/address/0x3A5129247306171ddC36ebCBC6e015bD9fd6f3a0) |

## V. Stargate Bridge Zap Deployed addresses

| Chain | Address
| ----------- | --------- |
| BSC | [0x251A3a5276b3DB89183362314B7fC4CFddE1df09](https://bscscan.com/address/0x251A3a5276b3DB89183362314B7fC4CFddE1df09) |
| Fantom | [0x390D0e521454301679f8D74a5B3a6cD9A7F0846e](https://ftmscan.com/address/0x390D0e521454301679f8D74a5B3a6cD9A7F0846e) |
| Avalanche | [0x85972d313BBd4C5EAEc36Dc93f78234db02f16c2](https://snowtrace.io/address/0x85972d313BBd4C5EAEc36Dc93f78234db02f16c2) |
| Polygon | [0x07FAb3964798150db677741EE51B836fAd0828E7](https://polygonscan.com/address/0x07FAb3964798150db677741EE51B836fAd0828E7) |
| Ethereum | [0xC4a2B9a86Fb928B0d300A1e6537cbb0ba7Eb47fe](https://etherscan.io/address/0xC4a2B9a86Fb928B0d300A1e6537cbb0ba7Eb47fe) |

## VI. CBridge Zap Deployed addresses

| Chain | Address
| ----------- | --------- |
| BSC | [0xA3F842D3208c8540553a807Cf7C0314A6Ad250c7](https://bscscan.com/address/0xA3F842D3208c8540553a807Cf7C0314A6Ad250c7) |
| Fantom | [0x46b8368b0dBe1E2BB7F2e20fa76d5af20a42eFBD](https://ftmscan.com/address/0x46b8368b0dBe1E2BB7F2e20fa76d5af20a42eFBD) |
| Avalanche | [0xd6dDCe16c1A536A1f7a36cBd786A013b129f939b](https://snowtrace.io/address/0xd6dDCe16c1A536A1f7a36cBd786A013b129f939b) |
| Polygon | [0x1E7Fd1ccAfF7a4f4b0E3C052eE7ae1bFe18da40D](https://polygonscan.com/address/0x1E7Fd1ccAfF7a4f4b0E3C052eE7ae1bFe18da40D) |
| Ethereum | [0x7c0Ab9bcF880c14d835e0f4B26725FEe8Cd5a65c](https://etherscan.io/address/0x7c0Ab9bcF880c14d835e0f4B26725FEe8Cd5a65c) |
