<div align="center">
  <h1>
    Liquid Crypto Bridge+ V2
  </h1>
</div>

## I. LC Bridge Deployed addresses

| Chain | Token | Address |
| ----- | ----- | ------- |
| BSC | BNB | [0x70DA2C70A34Ec07Aa14Ca20aAA08708E56C3ca8F](https://bscscan.com/address/0x70DA2C70A34Ec07Aa14Ca20aAA08708E56C3ca8F) |
|  | USDT | [0xB28706a7740bbcDae695ad80E96706e92879fAcB](https://bscscan.com/address/0xB28706a7740bbcDae695ad80E96706e92879fAcB) |
|  | JFIN | [0x97C337F9Af4b812B3B25d09D8FD198eFB5614724](https://bscscan.com/address/0x97C337F9Af4b812B3B25d09D8FD198eFB5614724) |
|  | OORTALPHA | [0x43fE54d5a3467A537529EEfd00D859ec3271bC1c](https://bscscan.com/address/0x43fE54d5a3467A537529EEfd00D859ec3271bC1c) |
|  | OORT | [0x4a456dB48EB4F8f83FB26278Cf7ee5563a232466](https://bscscan.com/address/0x4a456dB48EB4F8f83FB26278Cf7ee5563a232466) |
|  | LQDX | [0x4447de6dADa291faF2D54B295763B86a1d1ba61C](https://bscscan.com/address/0x4447de6dADa291faF2D54B295763B86a1d1ba61C) |
| Fantom | FTM | [0x8CE667A87bb1F2be51AC483427536502b6234cCA](https://ftmscan.com/address/0x8CE667A87bb1F2be51AC483427536502b6234cCA) |
| Avalanche | AVAX | [0x3Cfd108B06CbdC3F5d5B8A3dab1F4366D69aB912](https://snowtrace.io/address/0x3Cfd108B06CbdC3F5d5B8A3dab1F4366D69aB912) |
|  | LQDX | [0x8FbF1c2E49eeC070DA062E16F13b808BFe0a3EC2](https://snowtrace.io/address/0x8FbF1c2E49eeC070DA062E16F13b808BFe0a3EC2) |
| Polygon | Matic | [0xa15b948Df6Fc8091698Fd9353534e881d9aEF788](https://polygonscan.com/address/0xa15b948Df6Fc8091698Fd9353534e881d9aEF788) |
| Polygon | LQDX | [0x0ae17Bde6272EdD04272Eb0b005c73fdbA1611bA](https://polygonscan.com/address/0x0ae17Bde6272EdD04272Eb0b005c73fdbA1611bA) |
| Celo | CELO | [0xF06af63b86591b1ceC25107fE7E93Bdb235c0b6d](https://celoscan.io/address/0xF06af63b86591b1ceC25107fE7E93Bdb235c0b6d) |
| Cronos | CRO | [](https://cronoscan.com/address/) |
| Metis | METIS | [0x1d03D6a1DF335E26b0A5C895a8a8819179B693e8](https://andromeda-explorer.metis.io/address/0x1d03D6a1DF335E26b0A5C895a8a8819179B693e8/transactions) |
| Moonbeam | GLMR |  [](https://moonscan.io/address/) |
| Ethereum | ETH | [0xB37673d7CE08cA28f6e85E5DBAD424C869c1863C](https://etherscan.io/address/0xB37673d7CE08cA28f6e85E5DBAD424C869c1863C) |
|  | JFIN | [0x3EbFd0EFC49a27fb633bd56013E4220EBC2c3C6d](https://etherscan.io/address/0x3EbFd0EFC49a27fb633bd56013E4220EBC2c3C6d) |
|  | LQDX | [0x4F85E00dADDbf83233a6A3beB0EC5Bc460899d2f](https://etherscan.io/address/0x4F85E00dADDbf83233a6A3beB0EC5Bc460899d2f) |
| ONUS | ONUS | [0xc65675c182176BD7d910B6948BEf7D82508e81E2](https://explorer.onuschain.io/address/0xc65675c182176BD7d910B6948BEf7D82508e81E2/contracts) |
|  | LQDX | [0xcC6592a5dc34981346432A16Fe26f4DEDF630C3E](https://explorer.onuschain.io/address/0xcC6592a5dc34981346432A16Fe26f4DEDF630C3E/contracts) |
| Arbitrum | ETH | [0x744B02C41177d2138B0E8BAD39b39F9512c90b6d](https://arbiscan.io/address/0x744B02C41177d2138B0E8BAD39b39F9512c90b6d) |
| Boba | ETH | [0x283AC2e321d468E3AAD7bA347CaCBfd31806F79E](https://bobascan.com/address/0x283AC2e321d468E3AAD7bA347CaCBfd31806F79E) |
| Optimism | ETH | [0x8635811CddC0d0260525c6814355B076A37F1475](https://optimistic.etherscan.io/address/0x8635811CddC0d0260525c6814355B076A37F1475) |
| JFIN | JFIN | [0x306DfE76A4e9aBdF28a5fC38FE40118c16c80BEA](https://exp.jfinchain.com/address/0x306DfE76A4e9aBdF28a5fC38FE40118c16c80BEA) |
|  | LUSDT | [0x22EE6b89Ad95D457B493476FBdB24738E83da5EA](https://exp.jfinchain.com/address/0x22EE6b89Ad95D457B493476FBdB24738E83da5EA) |
|  | WIRTUAL | [0x57f37d1cc30b0C21aD6F6e5F9E1fDBb309dC4016](https://exp.jfinchain.com/address/0x57f37d1cc30b0C21aD6F6e5F9E1fDBb309dC4016) |
|  | LQDX | [0xFFcdd3B534259e31193629BF3b0E7A71436E049b](https://exp.jfinchain.com/address/0xFFcdd3B534259e31193629BF3b0E7A71436E049b) |
| Mantle | MNT | [0xE48c7d451Cc3Fe893FFaEBe050b80007abE650E7](https://explorer.mantle.xyz/address/0xE48c7d451Cc3Fe893FFaEBe050b80007abE650E7) |
|  | LQDX | [0x56C34d75e802799CF115C72371b82DF4ce1Fe1EE](https://mantle.socialscan.io/address/0x56C34d75e802799CF115C72371b82DF4ce1Fe1EE) |
| Base | ETH | [0x203ADf9D2AD4401dDDf53A3e5ba27c9437fd1Ab7](https://basescan.org/address/0x203ADf9D2AD4401dDDf53A3e5ba27c9437fd1Ab7) |
| OORT | OORT | [0xb14BE0420189EdBd0e49E315684596482B358DD3](https://mainnet-scan.oortech.com/address/0xb14BE0420189EdBd0e49E315684596482B358DD3) |
|  | LQDX | [0x2E592dF6c56a8720E46bB00D17f8FaB391BF97c8](https://mainnet-scan.oortech.com/address/0x2E592dF6c56a8720E46bB00D17f8FaB391BF97c8) |
| Arthera | ETH | [0x435c65C6b9b94acE75361Ce689067fDddAD1ccdD](https://explorer.arthera.net/address/0x435c65C6b9b94acE75361Ce689067fDddAD1ccdD) |
|  | LQDX | [0x8FbF1c2E49eeC070DA062E16F13b808BFe0a3EC2](https://explorer.arthera.net/address/0x8FbF1c2E49eeC070DA062E16F13b808BFe0a3EC2) |

## II. Bridge+ v2 Deployed addresses

| Chain | Address |
| ----- | ------- |
| BSC | [0xEF0a8F5D968aD988066b6899CB817f5f9Af9245f](https://bscscan.com/address/0xEF0a8F5D968aD988066b6899CB817f5f9Af9245f) |
| Fantom | [0x435c65C6b9b94acE75361Ce689067fDddAD1ccdD](https://ftmscan.com/address/0x435c65C6b9b94acE75361Ce689067fDddAD1ccdD) |
| Avalanche | [0x1c536F346e0bEB478f0E5C8E006c4583aC90AbE0](https://snowtrace.io/address/0x1c536F346e0bEB478f0E5C8E006c4583aC90AbE0) |
| Polygon | [0xC4469C21F3BD37dFfA1051B1397858AFF80D7A44](https://polygonscan.com/address/0xC4469C21F3BD37dFfA1051B1397858AFF80D7A44) |
| Celo | [0x3De4fB2033360598385d14A886d6B097f4209DC7](https://celoscan.io/address/0x3De4fB2033360598385d14A886d6B097f4209DC7) |
| Cronos | [](https://cronoscan.com/address/) |
| Metis | [0x2E592dF6c56a8720E46bB00D17f8FaB391BF97c8](https://andromeda-explorer.metis.io/address/0x2E592dF6c56a8720E46bB00D17f8FaB391BF97c8) |
| Moonbeam | [](https://moonscan.io/address/) |
| Ethereum | [0x4447de6dADa291faF2D54B295763B86a1d1ba61C](https://etherscan.io/address/0x4447de6dADa291faF2D54B295763B86a1d1ba61C) |
| ONUS | [0xBcB85Dc27db4C50d4617cc0A5a382489DC6E29D3](https://explorer.onuschain.io/address/0xBcB85Dc27db4C50d4617cc0A5a382489DC6E29D3/contracts) |
| Arbitrum | [0x802CD52CEf721d8935c27062B534fA7e65b04b4F](https://arbiscan.io/address/0x802CD52CEf721d8935c27062B534fA7e65b04b4F) |
| Boba | [0xb5ed4216cBfBE95AD9f59ce70b04184830D55043](https://bobascan.com/address/0xb5ed4216cBfBE95AD9f59ce70b04184830D55043) |
| Optimism | [0x364f17A23AE4350319b7491224d10dF5796190bC](https://optimistic.etherscan.io/address/0x364f17A23AE4350319b7491224d10dF5796190bC) |
| JFIN | [0xEb1cb0EbD1B12C79560eA9aB14cBd88F0D408d7a](https://exp.jfinchain.com/address/0xEb1cb0EbD1B12C79560eA9aB14cBd88F0D408d7a) |
| Mantle | [0x4447de6dADa291faF2D54B295763B86a1d1ba61C](https://explorer.mantle.xyz/address/0x4447de6dADa291faF2D54B295763B86a1d1ba61C) |
| Base | [0x5A03BCD8ca4d78Fe37ac558795803C1B8BE9378f](https://basescan.org/address/0x5A03BCD8ca4d78Fe37ac558795803C1B8BE9378f) |
| OORT | [0x367EbB688EfDdd9D27e349afdc760DF8bDB8C7f9](https://mainnet-scan.oortech.com/address/0x367EbB688EfDdd9D27e349afdc760DF8bDB8C7f9) | 10526242 |
| Arthera | [0x4F85E00dADDbf83233a6A3beB0EC5Bc460899d2f](https://explorer.arthera.net/address/0x4F85E00dADDbf83233a6A3beB0EC5Bc460899d2f) |

## III. Bridge+ v2 Insuarnace Deployed addresses

| Chain | Address |
| ----- | ------- |
| BSC | [0xfD9D2eB1eB464aC211e488dA68E4Fc4dCb9d02fb](https://bscscan.com/address/0xfD9D2eB1eB464aC211e488dA68E4Fc4dCb9d02fb) |
| Fantom | [0xC70A41ef01387a67C6E8f76a4a0159c331458c4b](https://ftmscan.com/address/0xC70A41ef01387a67C6E8f76a4a0159c331458c4b) |
| Avalanche | [0x553ae892d90ed6658E09BF215d9933F0E9254fd9](https://snowtrace.io/address/0x553ae892d90ed6658E09BF215d9933F0E9254fd9) |
| Polygon | [0x9CfcCd180C804367658Cc19b2b1bbCefbf540715](https://polygonscan.com/address/0x9CfcCd180C804367658Cc19b2b1bbCefbf540715) |
| Celo | [0xdA467b1FfAa488c76004e3e9575938f985Fe7611](https://celoscan.io/address/0xdA467b1FfAa488c76004e3e9575938f985Fe7611) |
| Cronos |  |
| Metis | [0xdA467b1FfAa488c76004e3e9575938f985Fe7611](https://andromeda-explorer.metis.io/address/0xdA467b1FfAa488c76004e3e9575938f985Fe7611) |
| Moonbeam |  |
| Ethereum | [0xA05d8e37c5B3FE871Ec40d171175b8572f1c0ACb](https://etherscan.io/address/0xA05d8e37c5B3FE871Ec40d171175b8572f1c0ACb) |
| ONUS | [0x8E976614b388b3B4a28abe4B04b81Df2B84e36cB](https://explorer.onuschain.io/address/0x8E976614b388b3B4a28abe4B04b81Df2B84e36cB/contracts) |
| Arbitrum | [0x27DD09b7CAD097d147B458c9900C4c0f8c08beC8](https://arbiscan.io/address/0x27DD09b7CAD097d147B458c9900C4c0f8c08beC8) |
| Boba | [0x1112eb329121F5513177C07729459A3Cf601f76d](https://bobascan.com/address/0x1112eb329121F5513177C07729459A3Cf601f76d) |
| Optimism | [0x6e9408aE39cC347F89Bd81DF4ac11998Ac09Ca3a](https://optimistic.etherscan.io/address/0x6e9408aE39cC347F89Bd81DF4ac11998Ac09Ca3a) |
| JFIN | [0xC06d675D84863162B604BE051E1E684b51A92D3c](https://exp.jfinchain.com/address/0xC06d675D84863162B604BE051E1E684b51A92D3c) |
| Mantle | [0x6D642253B6fD96d9D155279b57B8039675E49D8e](https://explorer.mantle.xyz/address/0x6D642253B6fD96d9D155279b57B8039675E49D8e) |
| Base | [0xaa3994895D1e4611a4C334969989Ff7cDF6A0764](https://basescan.org/address/0xaa3994895D1e4611a4C334969989Ff7cDF6A0764) |
| OORT | [0x8FbF1c2E49eeC070DA062E16F13b808BFe0a3EC2](https://mainnet-scan.oortech.com/address/0x8FbF1c2E49eeC070DA062E16F13b808BFe0a3EC2) |
| Arthera | [0x0866fAf786315a4d263215B5727A3e8d2Ea74D3E](https://explorer.arthera.net/address/0x0866fAf786315a4d263215B5727A3e8d2Ea74D3E) |
