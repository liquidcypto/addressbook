<div align="center">
  <h1>
    Liquid Crypto Jfin Feature
  </h1>
</div>

## I. JFIN Tokens

| Name | Address |
| ---- | ------- |
| WJFIN | [0x1112eb329121f5513177c07729459a3cf601f76d](https://exp.jfinchain.com/address/0x1112eb329121f5513177c07729459a3cf601f76d) |
| LUSDT | [0x1cfc7ee9f50c651759a1effcd058d84f42e26967](https://exp.jfinchain.com/address/0x1cfc7ee9f50c651759a1effcd058d84f42e26967) |

## II. JFIN DEX

| Name | Address |
| ---- | ------- |
| Factory | [0x8FbF1c2E49eeC070DA062E16F13b808BFe0a3EC2](https://exp.jfinchain.com/address/0x8FbF1c2E49eeC070DA062E16F13b808BFe0a3EC2) |
| Router01 | [0x47903bFF35Cb808ecbe545Bc4ccA1894F1ba3Be2](https://exp.jfinchain.com/address/0x47903bFF35Cb808ecbe545Bc4ccA1894F1ba3Be2) |
