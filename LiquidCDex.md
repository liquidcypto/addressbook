<div align="center">
  <h1>
    LiquidC DEX Factory and Router
  </h1>
</div>

## Smart contarct deployed address

| Chain | Name | Address |
| ----- | ---- | ------- |
| JFIN | Factory | [0x8FbF1c2E49eeC070DA062E16F13b808BFe0a3EC2](https://exp.jfinchain.com/address/0x8FbF1c2E49eeC070DA062E16F13b808BFe0a3EC2) |
|  | Router01 | [0x47903bFF35Cb808ecbe545Bc4ccA1894F1ba3Be2](https://exp.jfinchain.com/address/0x47903bFF35Cb808ecbe545Bc4ccA1894F1ba3Be2) |
|  | Router02 | [](https://exp.jfinchain.com/address/) |
|  | Rewarder | [0xefD39831544AF72FFED7B10CF3Af0c6583aEB002](https://exp.jfinchain.com/address/0xefD39831544AF72FFED7B10CF3Af0c6583aEB002) |
|  | Multicall | [0x397Ff8BDD96bD91c6C3A20AD233BF812B8C9Ba24](https://exp.jfinchain.com/address/0x397Ff8BDD96bD91c6C3A20AD233BF812B8C9Ba24) |
| ONUS | Factory | [0xd5d4e24eFeBEb9c89ce87e7Ba6Dc073626b86F1A](https://explorer.onuschain.io/address/0xd5d4e24eFeBEb9c89ce87e7Ba6Dc073626b86F1A) |
|  | Router01 | [0xB28706a7740bbcDae695ad80E96706e92879fAcB](https://explorer.onuschain.io/address/0xB28706a7740bbcDae695ad80E96706e92879fAcB) |
|  | Router02 |  |
|  | Rewarder | [0xc16ce7B683da825906c6CA8Df33986c6Ef9B287B](https://explorer.onuschain.io/address/0xc16ce7B683da825906c6CA8Df33986c6Ef9B287B) |
|  | Multicall | [0x69C692592Eb21678BCb9B2f6963E0B78669a150e](https://explorer.onuschain.io/address/0x69C692592Eb21678BCb9B2f6963E0B78669a150e) |
| BSC | Factory | [0x6D642253B6fD96d9D155279b57B8039675E49D8e](https://bscscan.com/address/0x6D642253B6fD96d9D155279b57B8039675E49D8e) |
|  | Router01 | [0xBcB85Dc27db4C50d4617cc0A5a382489DC6E29D3](https://bscscan.com/address/0xBcB85Dc27db4C50d4617cc0A5a382489DC6E29D3) |
|  | Router02 |  |
|  | Rewarderv2 | [0x230331584038e07835025A39867262DAA297854b](https://bscscan.com/address/0x230331584038e07835025A39867262DAA297854b) |
|  | Multicall | [0xc082bdDf585F1bCbed0dEe95a61A4A22b993505D](https://bscscan.com/address/0xc082bdDf585F1bCbed0dEe95a61A4A22b993505D) |
| Ethrereum | Factory | [0xBC7D212939FBe696e514226F3FAfA3697B96Bf59](https://etherscan.io/address/0xBC7D212939FBe696e514226F3FAfA3697B96Bf59) |
|  | Router01 | [0xeba22665C355457FA4e5A07B3A7559F22fd74923](https://etherscan.io/address/0xeba22665C355457FA4e5A07B3A7559F22fd74923) |
|  | Router02 |  |
|  | Rewarder | [0x21e6fA0156a91142342469f75278a1A715d46691](https://etherscan.io/address/0x21e6fA0156a91142342469f75278a1A715d46691) |
|  | Multicall | [0x85E959c76E5CAe8F6ed6621aBcdc023b4dAbB1EF](https://etherscan.io/address/0x85E959c76E5CAe8F6ed6621aBcdc023b4dAbB1EF) |
| Avalanche | Factory | [0x91a60E98AF717912C87b661FBF9e7C7B5Ba7DECD](https://snowtrace.io/address/0x91a60E98AF717912C87b661FBF9e7C7B5Ba7DECD) |
|  | Router01 | [0x1614C566340FB500Fea9aBC324cB89a1A7614868](https://snowtrace.io/address/0x1614C566340FB500Fea9aBC324cB89a1A7614868) |
|  | Router02 |  |
|  | Rewarder | [0x4be2814F123DeA37aeE4ee5e3b415ea58Ea16101](https://snowtrace.io/address/0x4be2814F123DeA37aeE4ee5e3b415ea58Ea16101) |
|  | Multicall | [0x18C0B83bDaEEC719C31226DdCc13bDBB83151b51](https://snowtrace.io/address/0x18C0B83bDaEEC719C31226DdCc13bDBB83151b51) |
| Mantle | Factory | [0xd9B20e1E9C34dD8784f1B39A29D0dFE38DC2DAdC](https://mantle.socialscan.io/address/0xd9B20e1E9C34dD8784f1B39A29D0dFE38DC2DAdC) |
|  | Router01 | [0x03D70deb5e85eCb26843F305798a183fb34465FC](https://mantle.socialscan.io/address/0x03D70deb5e85eCb26843F305798a183fb34465FC) |
|  | Router02 |  |
|  | Rewarder | [0xa6Ad5372E46bFE1CB5691c089901ba61d3B94143](https://mantle.socialscan.io/address/0xa6Ad5372E46bFE1CB5691c089901ba61d3B94143) |
|  | Multicall | [0x07ce406b975b3e15a0eeFF25E617873317853655](https://explorer.mantle.xyz/address/0x07ce406b975b3e15a0eeFF25E617873317853655) |
| OORT | Factory | [0xd5d4e24eFeBEb9c89ce87e7Ba6Dc073626b86F1A](https://mainnet-scan.oortech.com/address/0xd5d4e24eFeBEb9c89ce87e7Ba6Dc073626b86F1A) |
|  | Router01 | [0xcC6592a5dc34981346432A16Fe26f4DEDF630C3E](https://mainnet-scan.oortech.com/address/0xcC6592a5dc34981346432A16Fe26f4DEDF630C3E) |
|  | Router02 |  |
|  | Rewarder | [0x0802dE6AF6bDe9e5938eb05323aa00314f69594a](https://mainnet-scan.oortech.com/address/0x0802dE6AF6bDe9e5938eb05323aa00314f69594a) |
|  | Multicall | [0xa7487a75C49a32e8BA680b93851b97dE16E5dc18](https://mainnet-scan.oortech.com/address/0xa7487a75C49a32e8BA680b93851b97dE16E5dc18) |
| Polygon | Factory | [0xaa61A0223caD2EB0dfc5B8c1456d76e1702d45D1](https://polygonscan.com/address/0xaa61A0223caD2EB0dfc5B8c1456d76e1702d45D1) |
|  | Router01 | [0xE60bc20Aaf7b1A4851247A32f2B0316C195AD3e3](https://polygonscan.com/address/0xE60bc20Aaf7b1A4851247A32f2B0316C195AD3e3) |
|  | Router02 |  |
|  | Rewarder | [0xF625a0498adf3AAffC78dcAC142a815Fe8DA2BFc](https://polygonscan.com/address/0xF625a0498adf3AAffC78dcAC142a815Fe8DA2BFc) |
|  | Multicall | [0x1Cfc7EE9f50C651759a1EfFCd058D84F42E26967](https://polygonscan.com/address/0x1Cfc7EE9f50C651759a1EfFCd058D84F42E26967) |
| optimism | Factory | [0x47903bFF35Cb808ecbe545Bc4ccA1894F1ba3Be2](https://optimistic.etherscan.io/address/0x47903bFF35Cb808ecbe545Bc4ccA1894F1ba3Be2) |
|  | Router01 | [0x27DD09b7CAD097d147B458c9900C4c0f8c08beC8](https://optimistic.etherscan.io/address/0x27DD09b7CAD097d147B458c9900C4c0f8c08beC8) |
|  | Router02 |  |
|  | Rewarder | [0xCB124eb90517b8e51FE090701aE1cd436660517B](https://optimistic.etherscan.io/address/0xCB124eb90517b8e51FE090701aE1cd436660517B) |
|  | Multicall | [0x36a426BC3A6A6BF2208149B19De95e9195413C45](https://optimistic.etherscan.io/address/0x36a426BC3A6A6BF2208149B19De95e9195413C45) |
| Arthera | Factory | [0xd5d4e24eFeBEb9c89ce87e7Ba6Dc073626b86F1A](https://explorer.arthera.net/address/0xd5d4e24eFeBEb9c89ce87e7Ba6Dc073626b86F1A) | 5384098 |
|  | Router01 | [0xB28706a7740bbcDae695ad80E96706e92879fAcB](https://explorer.arthera.net/address/0xB28706a7740bbcDae695ad80E96706e92879fAcB) |
|  | Router02 |  |
|  | Rewarderv2 | [0x3De4fB2033360598385d14A886d6B097f4209DC7](https://explorer.arthera.net/address/0x3De4fB2033360598385d14A886d6B097f4209DC7) |
|  | Executor | [0xdA467b1FfAa488c76004e3e9575938f985Fe7611](https://explorer.arthera.net/address/0xdA467b1FfAa488c76004e3e9575938f985Fe7611) |

## DexZap Smart contarct deployed address

| Chain | Name | Address |
| ----- | ---- | ------- |
| JFIN | JFXv2Zap | [0xF551c44D621aEDBdccfDecD5C1a5517B31034fCd](https://exp.jfinchain.com/address/0xF551c44D621aEDBdccfDecD5C1a5517B31034fCd) |
| ONUS | LiquidXv2Zap | [0xC849AC003F2Ff5c275027e90bf2C2169a08359F8](https://explorer.onuschain.io/address/0xC849AC003F2Ff5c275027e90bf2C2169a08359F8) |
| BSC | LiquidXv2Zap | [0x65E6f1E055E9d6EBE06F48b3Ef890acd4a467C1C](https://bscscan.com/address/0x65E6f1E055E9d6EBE06F48b3Ef890acd4a467C1C) |
| Ethereum | LiquidXv2Zap | [0xDC7667072c70c2Ba4c5E806a2aF26002aC6426E1](https://etherscan.io/address/0xDC7667072c70c2Ba4c5E806a2aF26002aC6426E1) |
| Avalanche | LiquidXv2Zap | [0xA767A908Ed4DBf2C029FE3DDaC059dEB3B0a9045](https://snowtrace.io/address/0xA767A908Ed4DBf2C029FE3DDaC059dEB3B0a9045) |
| Mantle | LiquidXv2Zap | [0x9C57fd52D2f8ad73FBB2FAc91473D51e80F377BA](https://mantle.socialscan.io/address/0x9C57fd52D2f8ad73FBB2FAc91473D51e80F377BA) |
| OORT | LiquidXv2Zap | [0x85E959c76E5CAe8F6ed6621aBcdc023b4dAbB1EF](https://mainnet-scan.oortech.com/address/0x85E959c76E5CAe8F6ed6621aBcdc023b4dAbB1EF) |
| Polygon | LiquidXv2Zap | [0xf6b9EaFfBc8aC294102f93A661E3075C721f7a6a](https://polygonscan.com/address/0xf6b9EaFfBc8aC294102f93A661E3075C721f7a6a) |
| optimism | LiquidXv2Zap | [0x0534BB9d4E28b25b1e965E315afb5cAac968F643](https://optimistic.etherscan.io/address/0x0534BB9d4E28b25b1e965E315afb5cAac968F643) |
| Arthera | LiquidXv2Zap | [0x2E592dF6c56a8720E46bB00D17f8FaB391BF97c8](https://explorer.arthera.net/address/0x2E592dF6c56a8720E46bB00D17f8FaB391BF97c8) |
