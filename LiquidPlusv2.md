<div align="center">
  <h1>
    Liquid Crypto Liquid+ V2
  </h1>
</div>

## I. Which is different is there between version 1 and 2?

Version 2 manage all pools in one contract instead of version 1 has got a contract for a pool.

## II. Definition

`Pool` contract control assets flow with other staking platforms.

`Ledger` contract manage user's stake balance.

## III. Smart contarct deployed address

| Chain | Platform | Name | Address |
| ----- | -------- | ---- | ------- |
| BSC | PancakeSwap V3 | Pool | [0x9b0EaC1494433e08e4Ab9d644A95b39B2dcc41DB](https://bscscan.com/address/0x9b0EaC1494433e08e4Ab9d644A95b39B2dcc41DB) |
|  |  | Ledger | [0x168a6f6331948350c6b4F51444E2C5A87fF85D0E](https://bscscan.com/address/0x168a6f6331948350c6b4F51444E2C5A87fF85D0E) |
|  | PancakeSwap V2 | Pool | [0x8f3CF19982CaE4505dCBa8C51116b2C45d5AC2D1](https://bscscan.com/address/0x8f3CF19982CaE4505dCBa8C51116b2C45d5AC2D1) |
|  |  | Ledger | [0x50dE0D2632119AeD31E1CC9C7E530588c68A2382](https://bscscan.com/address/0x50dE0D2632119AeD31E1CC9C7E530588c68A2382) |
|  | BiSwap | Pool | [0x9926998DC49E943B2a837E5D35142344a5A09500](https://bscscan.com/address/0x9926998DC49E943B2a837E5D35142344a5A09500) |
|  |  | Ledger | [0xe11361F0F3CE52c859b04284458A91f5faE4e0Ac](https://bscscan.com/address/0xe11361F0F3CE52c859b04284458A91f5faE4e0Ac) |
|  | UniSwap V3 | Pool | [0xaF4d854F7Be44843d26Db88B565E4ecD541b71d7](https://bscscan.com/address/0xaF4d854F7Be44843d26Db88B565E4ecD541b71d7) |
|  |  | Ledger | [0xaFCE5cab89343ce92F2bc017b040a22ae2f462a4](https://bscscan.com/address/0xaFCE5cab89343ce92F2bc017b040a22ae2f462a4) |
| Fantom | SpookySwap v21 | Pool | [0x3d3c1C878365E7fb6C038B13B0978D1F64A0d79f](https://ftmscan.com/address/0x3d3c1C878365E7fb6C038B13B0978D1F64A0d79f) |
|  |  | Ledger | [0x2F2006889c0f44324b56C6927c8c434799D673d0](https://ftmscan.com/address/0x2F2006889c0f44324b56C6927c8c434799D673d0) |
|  | SpookySwap v22 | Pool | [0x1f1F4212999b4b7e51d7760B42CB586b8BAb2D2f](https://ftmscan.com/address/0x1f1F4212999b4b7e51d7760B42CB586b8BAb2D2f) |
|  |  | Ledger | [0xB64EF13719ec385D67d366AdaE8663Faf4157a2d](https://ftmscan.com/address/0xB64EF13719ec385D67d366AdaE8663Faf4157a2d) |
|  | WigoSwap | Pool | [0xFA8672c27b59eA6f8247F414C463f41718B27d87](https://ftmscan.com/address/0xFA8672c27b59eA6f8247F414C463f41718B27d87) |
|  |  | Ledger | [0x5FF768769931F2fCa77108a849a10Ba138610a45](https://ftmscan.com/address/0x5FF768769931F2fCa77108a849a10Ba138610a45) |
|  | JetSwap | Pool | [0x115562921825C5b10A755B03e67A60E8a723A74e](https://ftmscan.com/address/0x115562921825C5b10A755B03e67A60E8a723A74e) |
|  |  | Ledger | [0xd92A31eDbD209A8eA2d4C71279690b4fAF76d837](https://ftmscan.com/address/0xd92A31eDbD209A8eA2d4C71279690b4fAF76d837) |
| Avalanche | Pangolin | Pool | [0x77cCcDf98806F863984561024AeF646e1df7cc20](https://snowtrace.io/address/0x77cCcDf98806F863984561024AeF646e1df7cc20) |
|  |  | Ledger | [0x91923dC6aB6F50705E661bC94E8Cb00ea1075D9A](https://snowtrace.io/address/0x91923dC6aB6F50705E661bC94E8Cb00ea1075D9A) |
| Polygon | Uniswap V3 | Pool | [0x10e056a114342a773Fec46fBa24d2052c7008940](https://polygonscan.com/address/0x10e056a114342a773Fec46fBa24d2052c7008940) |
|  |  | Ledger | [0x98ab305e0e76D43d0Ae370869A69c6C0E23CB492](https://polygonscan.com/address/0x98ab305e0e76D43d0Ae370869A69c6C0E23CB492) |
|  | JetSwap | Pool | [0x2C1a6286b76196B8C17f63C30427Db204D417A45](https://polygonscan.com/address/0x2C1a6286b76196B8C17f63C30427Db204D417A45) |
|  |  | Ledger | [0x19Bfb765Dae1B3B49C9ed141Df7A0Dc55080fA19](https://polygonscan.com/address/0x19Bfb765Dae1B3B49C9ed141Df7A0Dc55080fA19) |
| Celo | Uniswap V3 | Pool | [0x5C516aC15537909d3DDd7e890C065D1c27fb49C3](https://celoscan.io/address/0x5C516aC15537909d3DDd7e890C065D1c27fb49C3) |
|  |  | Ledger | [0xd4c470e9B591B3f24B14aB4A161Af1FAe7df4516](https://celoscan.io/address/0xd4c470e9B591B3f24B14aB4A161Af1FAe7df4516) |
| Cronos |  |  | [](https://cronoscan.com/address/) |
| Metis | Netswap | Pool | [0x1225EB759104090fA7159C8D62D478251B888378](https://andromeda-explorer.metis.io/address/0x1225EB759104090fA7159C8D62D478251B888378/transactions) |
|  |  | Ledger | [0x21f6b3Ac430d1e3d0134A82964E083E29Defb62b](https://andromeda-explorer.metis.io/address/0x21f6b3Ac430d1e3d0134A82964E083E29Defb62b/transactions) |
| Moonbeam |  |  | [](https://moonscan.io/address/) |
| Ethereum | Uniswap V3 | Pool | [0xCcF388B4beb67A2081B265d14f1a124604D450F3](https://etherscan.io/address/0xCcF388B4beb67A2081B265d14f1a124604D450F3) |
|  |  | Ledger | [0xd5429bF56A3EedfbeCf6818CDebf3FCB8CCb5869](https://etherscan.io/address/0xd5429bF56A3EedfbeCf6818CDebf3FCB8CCb5869) |
|  | Sushiswap | Pool | [0x22011BF1a77a618e3c6bB1C0d134f72AD77D4184](https://etherscan.io/address/0x22011BF1a77a618e3c6bB1C0d134f72AD77D4184) |
|  |  | Ledger | [0xDD151740aE207de198Af1833C97333A6904E6aFe](https://etherscan.io/address/0xDD151740aE207de198Af1833C97333A6904E6aFe) |
|  | PancakeSwap | Pool | [0x5B9d5fD511F1647659cAb969847c7f8fc4557Cb0](https://etherscan.io/address/0x5B9d5fD511F1647659cAb969847c7f8fc4557Cb0) |
|  |  | Ledger | [0x18387dFC5Cf69164aEB086E8Fa5aA29a0Dc44fB2](https://etherscan.io/address/0x18387dFC5Cf69164aEB086E8Fa5aA29a0Dc44fB2) |
| ONUS | MiaSwap | Pool | [0xb25ecE2227B4c53810Ca216777159832f48D4158](https://explorer.onuschain.io/address/0xb25ecE2227B4c53810Ca216777159832f48D4158/contracts) |
|  |  | Ledger | [0x7c0Ab9bcF880c14d835e0f4B26725FEe8Cd5a65c](https://explorer.onuschain.io/address/0x7c0Ab9bcF880c14d835e0f4B26725FEe8Cd5a65c/contracts) |
|  | HeraSwap | Pool | [0x5940cFbF45F9b044B9c346ac26C830272E7Eb3bE](https://explorer.onuschain.io/address/0x5940cFbF45F9b044B9c346ac26C830272E7Eb3bE/contracts) |
|  |  | Ledger | [0x16CEA5da84d84916DBaEeDeEF91620D044d88A40](https://explorer.onuschain.io/address/0x16CEA5da84d84916DBaEeDeEF91620D044d88A40/contracts) |
| Arbitrum | Uniswap V3 | Pool | [0xBcF79fbfC58b06a71F37d6EF004dc46B58BF9d20](https://arbiscan.io/address/0xBcF79fbfC58b06a71F37d6EF004dc46B58BF9d20) |
|  |  | Ledger | [0x082A1a79AA35dBF3d6880E3b3E5FfEC349b08dAE](https://arbiscan.io/address/0x082A1a79AA35dBF3d6880E3b3E5FfEC349b08dAE) |
|  | SushiSwap | Pool | [0x2CB20BF05B6C48DCE3B3db3Be6a74D53416a028B](https://arbiscan.io/address/0x2CB20BF05B6C48DCE3B3db3Be6a74D53416a028B) |
|  |  | Ledger | [0x823702aFD28aeE5f8fab855623BC11666A7AE2BE](https://arbiscan.io/address/0x823702aFD28aeE5f8fab855623BC11666A7AE2BE) |
| Boba | OolongSwap | Pool | [0xc809FD29f81Ea18408Cf21Df896b26f01a309Bc7](https://bobascan.com/address/0xc809FD29f81Ea18408Cf21Df896b26f01a309Bc7) |
|  |  | Ledger | [0x69F4c039Af583c881161a5FeF24EdBC1a98d3BF5](https://bobascan.com/address/0x69F4c039Af583c881161a5FeF24EdBC1a98d3BF5) |
| Optimism | Uniswap V3 | Pool | [0x1C85169D60A4339D5c014226c21D7b49547eaA95](https://optimistic.etherscan.io/address/0x1C85169D60A4339D5c014226c21D7b49547eaA95) |
|  |  | Ledger | [0x4d4442Bd5b7a7721794cc98F753BFCB66De590AE](https://optimistic.etherscan.io/address/0x4d4442Bd5b7a7721794cc98F753BFCB66De590AE) |
| JFIN | JFXv2 | Factory | [0x8FbF1c2E49eeC070DA062E16F13b808BFe0a3EC2](https://exp.jfinchain.com/address/0x8FbF1c2E49eeC070DA062E16F13b808BFe0a3EC2) |
|  |  | Router01 | [0x47903bFF35Cb808ecbe545Bc4ccA1894F1ba3Be2](https://exp.jfinchain.com/address/0x47903bFF35Cb808ecbe545Bc4ccA1894F1ba3Be2) |
|  |  | Zap | [0x997D25AAb6957802631c359Bd1CB06Ca286B0D10](https://exp.jfinchain.com/address/0x997D25AAb6957802631c359Bd1CB06Ca286B0D10) |
| Mantle | iZUMi | Pool | [0xA767A908Ed4DBf2C029FE3DDaC059dEB3B0a9045](https://explorer.mantle.xyz/address/0xA767A908Ed4DBf2C029FE3DDaC059dEB3B0a9045) |
|  |  | Ledger | [0x3efFd75A021f6aF5E7bD265932CC4074E6e64bC7](https://explorer.mantle.xyz/address/0x3efFd75A021f6aF5E7bD265932CC4074E6e64bC7) |
