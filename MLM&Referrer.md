<div align="center">
  <h1>
    MLM & Referral Code
  </h1>
</div>

## I. How to split reward from LiquidC pools?

<img src="mlm/1.png" alt="drawing" style="max-width:800px;"/>

This platform get revenue from rewards from user's staking and deposit/withdraw fee.
This revenue is managed by `FeeTierStrate` smart contract.
1. When user depoist/withdraw crypto into LiquidC pools, platform can earn fee.
2. When user stake crypto, reward token supplied to this platform from other DEXs.
`FeeTierStrate` contract manage how to split this reward between investor and platform.
Also, this contract manage how to split reward to MLM users.
3. User can share profit from platform to others via referral code. This referral reward is managed by `ReferrerTree` contract.

## II. `FeeTierStrate` smart contarct deployed address

| Chain | Address
| ----------- | --------- |
| BSC | [0xdd43fAd5F91a16990C9290f84d893bC1a28Fc164](https://bscscan.com/address/0xdd43fAd5F91a16990C9290f84d893bC1a28Fc164) |
| Fantom | [0x4d4442Bd5b7a7721794cc98F753BFCB66De590AE](https://ftmscan.com/address/0x4d4442Bd5b7a7721794cc98F753BFCB66De590AE) |
| Avalanche | [0x1F735C88a5037de2Adf92bC513417a25C5732d95](https://snowtrace.io/address/0x1F735C88a5037de2Adf92bC513417a25C5732d95) |
| Polygon | [0xf9413D93afc2A5192B564009A92F2c1d98535C14](https://polygonscan.com/address/0xf9413D93afc2A5192B564009A92F2c1d98535C14) |
| Celo | [0x667Ac182Be13aa8dda7ea91af2AaB7327EE0a1b4](https://celoscan.io/address/0x667Ac182Be13aa8dda7ea91af2AaB7327EE0a1b4) |
| Cronos | [0x667Ac182Be13aa8dda7ea91af2AaB7327EE0a1b4](https://cronoscan.com/address/0x667Ac182Be13aa8dda7ea91af2AaB7327EE0a1b4) |
| Metis | [0x8635811CddC0d0260525c6814355B076A37F1475](https://andromeda-explorer.metis.io/address/0x8635811CddC0d0260525c6814355B076A37F1475/transactions) |
| Moonbeam | [0xF9ff9AAdE42E245b3FE07894ca86ad5E4953D036](https://moonscan.io/address/0xF9ff9AAdE42E245b3FE07894ca86ad5E4953D036) |
| Ethereum | [0x4d4442Bd5b7a7721794cc98F753BFCB66De590AE](https://etherscan.io/address/0x4d4442Bd5b7a7721794cc98F753BFCB66De590AE) |

## III. `ReferrerTree` smart contarct deployed address

| Chain | Address
| ----------- | --------- |
| BSC | [0x5298fEb14453fF5c87014C76BE12a442045a1Fa4](https://bscscan.com/address/0x5298fEb14453fF5c87014C76BE12a442045a1Fa4) |
| Fantom | [0x9e431Bc0A3417e827155571605d4df22E43e1bdc](https://ftmscan.com/address/0x9e431Bc0A3417e827155571605d4df22E43e1bdc) |
| Avalanche | [0x38F49694235d86b55D39a4b78CD717fb06357c3D](https://snowtrace.io/address/0x38F49694235d86b55D39a4b78CD717fb06357c3D) |
| Polygon | [0x5d5D6AeA4ddC98Cc9FF6bf52912BCF5D320703E2](https://polygonscan.com/address/0x5d5D6AeA4ddC98Cc9FF6bf52912BCF5D320703E2) |
| Ethereum | [0x9F91141Fb28abc8D088f6B455576B212d2c3500f](https://etherscan.io/address/0x9F91141Fb28abc8D088f6B455576B212d2c3500f) |

## IV. `FeeTierStrate v2` smart contarct deployed address

Version 2 is almost same as version 1.
But in version 1, LiquidC pools and baskets are splited reward in same role, but in version 2 admin user can manage different role in LiquidC pools and baskets.

| Chain | Address
| ----------- | --------- |
| BSC | [0x8073a9e2ba96b0281881ab66e65667bdca4d077f](https://bscscan.com/address/0x8073a9e2ba96b0281881ab66e65667bdca4d077f) |
| Fantom | [0x509bC69b8F84Dcf093879698F24D99fAAcf7e590](https://ftmscan.com/address/0x509bC69b8F84Dcf093879698F24D99fAAcf7e590) |
| Avalanche | [0x72966b83f31C7F54E5fA956E603450dA93d622d0](https://snowtrace.io/address/0x72966b83f31C7F54E5fA956E603450dA93d622d0) |
| Polygon | [0x0dfc2BFdA4dB61E41792de373F3d2d23a9068090](https://polygonscan.com/address/0x0dfc2BFdA4dB61E41792de373F3d2d23a9068090) |
| Celo | [0x66eFa83e317e91B4F537EdC53F302A6B25880988](https://celoscan.io/address/0x66eFa83e317e91B4F537EdC53F302A6B25880988) |
| Cronos | [](https://cronoscan.com/address/) |
| Metis | [0xB52E3912706e135ad65431D536d6aF9e71A9F1f3](https://andromeda-explorer.metis.io/address/0xB52E3912706e135ad65431D536d6aF9e71A9F1f3/transactions) |
| Moonbeam | [](https://moonscan.io/address/) |
| Ethereum | [0x471200CE99a9608b5676660c98d31167825Af1DE](https://etherscan.io/address/0x471200CE99a9608b5676660c98d31167825Af1DE) |
| ONUS | [0xDd6bCdFD60F4E24F33b821CcCc11ee9cD135Dd84](https://explorer.onuschain.io/address/0xDd6bCdFD60F4E24F33b821CcCc11ee9cD135Dd84/contracts) |
| Arbitrum | [0x6B5B376D466e39Fd30878617049dfD0FA8f49eF1](https://arbiscan.io/address/0x6B5B376D466e39Fd30878617049dfD0FA8f49eF1) |
| Boba | [0xBbFC95dDc45e76dd25b222Ef1f46396a5b283c98](https://bobascan.com/address/0xBbFC95dDc45e76dd25b222Ef1f46396a5b283c98) |
| Optimism | [0x1EdFEf6e7CB001aFA338ce167434e1f6dd846f0e](https://optimistic.etherscan.io/address/0x1EdFEf6e7CB001aFA338ce167434e1f6dd846f0e) |
| JFIN | [](https://exp.jfinchain.com/address/) |
| Mantle | [0x8FbF1c2E49eeC070DA062E16F13b808BFe0a3EC2](https://explorer.mantle.xyz/address/0x8FbF1c2E49eeC070DA062E16F13b808BFe0a3EC2) |
