<div align="center">
  <h1>
    Liquid Crypto Swap+
  </h1>
</div>

## I. Waht is Swap+?

Swap+ aggregate all pair liquidity DEXs in chain, recommend best route to swap assets.

## II. Smart contarct deployed address

| Chain | Address |
| ----- | ------- |
| BSC | [0x4444457406B815253080BC38B084aA1A14FaadBF](https://bscscan.com/address/0x4444457406B815253080BC38B084aA1A14FaadBF) |
| Fantom | [0x998E000E3928a649A94C61695dE93dEFA1144fB6](https://ftmscan.com/address/0x998E000E3928a649A94C61695dE93dEFA1144fB6) |
| Avalanche | [0x5cb7Df8b5f539354F6B2e48F9C487D2C4B6B5db3](https://snowtrace.io/address/0x5cb7Df8b5f539354F6B2e48F9C487D2C4B6B5db3) |
| Polygon | [0x2f349B6AC07616D7865c1aA3eFB61a09400D9452](https://polygonscan.com/address/0x2f349B6AC07616D7865c1aA3eFB61a09400D9452) |
| Celo | [0x3Cc3d8eE5c0DEb0F08bd8Be1FBD1A0dc604a7b89](https://celoscan.io/address/0x3Cc3d8eE5c0DEb0F08bd8Be1FBD1A0dc604a7b89) |
| Cronos | [](https://cronoscan.com/address/) |
| Metis | [0x30A7cDd896C5EDB93aB696ced1b447C57313EFc7](https://andromeda-explorer.metis.io/address/0x30A7cDd896C5EDB93aB696ced1b447C57313EFc7/transactions) |
| Moonbeam | [](https://moonscan.io/address/) |
| Ethereum | [0x91f6AA43c190E317E4D02698127aA7A629CF4b23](https://etherscan.io/address/0x91f6AA43c190E317E4D02698127aA7A629CF4b23) |
| ONUS | [0x78345C5Bba1C571BFb681cf7244ddfc1D780f864](https://explorer.onuschain.io/address/0x78345C5Bba1C571BFb681cf7244ddfc1D780f864/contracts) |
| Arbitrum | [0x1225EB759104090fA7159C8D62D478251B888378](https://arbiscan.io/address/0x1225EB759104090fA7159C8D62D478251B888378) |
| Boba | [0x0978A07664972fc305e06759928F50615355B63F](https://bobascan.com/address/0x0978A07664972fc305e06759928F50615355B63F) |
| Optimism | [0x278fF09B6c3Ab6bC80D1aC32f629CB6DC2388F4C](https://optimistic.etherscan.io/address/0x278fF09B6c3Ab6bC80D1aC32f629CB6DC2388F4C) |
| JFIN | [0x64C5AF99bbF4C9DE53ba228ddd173651482d6415](https://exp.jfinchain.com/address/0x64C5AF99bbF4C9DE53ba228ddd173651482d6415) |
| Mantle | [0x3De4fB2033360598385d14A886d6B097f4209DC7](https://explorer.mantle.xyz/address/0x3De4fB2033360598385d14A886d6B097f4209DC7) |
| Base | [0x8FbF1c2E49eeC070DA062E16F13b808BFe0a3EC2](https://basescan.org/address/0x8FbF1c2E49eeC070DA062E16F13b808BFe0a3EC2) |
| OORT | [0x9C3a365b2b526Bb290A30005dd6bDC50bd1C9b5e](https://mainnet-scan.oortech.com/address/0x9C3a365b2b526Bb290A30005dd6bDC50bd1C9b5e) |
| Arthera | [0x4c630c1e722d2d7A0B749F1a05Da588725F8a1A3](https://explorer.arthera.net/address/0x4c630c1e722d2d7A0B749F1a05Da588725F8a1A3) |

## III. Insuarance contract address

| Chain | Address |
| ----- | ------- |
| BSC | [0x3366b89a4BFB8af11729Fe00d104d00E0Ee8C7f9](https://bscscan.com/address/0x3366b89a4BFB8af11729Fe00d104d00E0Ee8C7f9) |
| Fantom | [0x2E592dF6c56a8720E46bB00D17f8FaB391BF97c8](https://ftmscan.com/address/0x2E592dF6c56a8720E46bB00D17f8FaB391BF97c8) |
| Avalanche | [0x96b665033eBA9dE392cb5044E726C8Df42BA7d4D](https://snowtrace.io/address/0x96b665033eBA9dE392cb5044E726C8Df42BA7d4D) |
| Polygon | [0x47903bFF35Cb808ecbe545Bc4ccA1894F1ba3Be2](https://polygonscan.com/address/0x47903bFF35Cb808ecbe545Bc4ccA1894F1ba3Be2) |
| Celo | [0xB28706a7740bbcDae695ad80E96706e92879fAcB](https://celoscan.io/address/0xB28706a7740bbcDae695ad80E96706e92879fAcB) |
| Cronos |  |
| Metis | [0xB28706a7740bbcDae695ad80E96706e92879fAcB](https://andromeda-explorer.metis.io/address/0xB28706a7740bbcDae695ad80E96706e92879fAcB) |
| Moonbeam |  |
| Ethereum | [0x3ADE0CD04B0836e3135e53877543574E3426C128](https://etherscan.io/address/0x3ADE0CD04B0836e3135e53877543574E3426C128) |
| ONUS | [0xA3327FfC9087F221D0573Ed7916dE8EbCD83f714](https://explorer.onuschain.io/address/0xA3327FfC9087F221D0573Ed7916dE8EbCD83f714/contracts) |
| Arbitrum | [0x9CfcCd180C804367658Cc19b2b1bbCefbf540715](https://arbiscan.io/address/0x9CfcCd180C804367658Cc19b2b1bbCefbf540715) |
| Boba | [0x3De4fB2033360598385d14A886d6B097f4209DC7](https://bobascan.com/address/0x3De4fB2033360598385d14A886d6B097f4209DC7) |
| Optimism | [0xC70A41ef01387a67C6E8f76a4a0159c331458c4b](https://optimistic.etherscan.io/address/0xC70A41ef01387a67C6E8f76a4a0159c331458c4b) |
| JFIN | [0x73eB5Be205e7b9AF41Cb735d5Ee1551dC1650193](https://exp.jfinchain.com/address/0x73eB5Be205e7b9AF41Cb735d5Ee1551dC1650193) |
| Mantle | [0xdAd73211C1cB921f91D16f87b3b9e16bcE1Fd886](https://explorer.mantle.xyz/address/0xdAd73211C1cB921f91D16f87b3b9e16bcE1Fd886) |
| Base | [0x21e6fA0156a91142342469f75278a1A715d46691](https://basescan.org/address/0x21e6fA0156a91142342469f75278a1A715d46691) |
| OORT | [0x435c65C6b9b94acE75361Ce689067fDddAD1ccdD](https://mainnet-scan.oortech.com/address/0x435c65C6b9b94acE75361Ce689067fDddAD1ccdD) |
| Arthera | [0xfb33944B3Fe7756628B40DD73cBC854e5F136D19](https://explorer.arthera.net/address/0xfb33944B3Fe7756628B40DD73cBC854e5F136D19) |
